const TestRailApi = require('./testrail');

class TestRailSestraReporter {
  constructor(emitter, reporterOptions) {
      // Required
      this.host = process.env.TESTRAIL_HOST || reporterOptions.testrailSestraHost || '';
      this.user = process.env.TESTRAIL_USERNAME || reporterOptions.testrailSestraUsername || '';
      this.password = process.env.TESTRAIL_PASSWORD || process.env.TESTRAIL_API_KEY || reporterOptions.testrailSestraPassword || '';
      this.projectId = process.env.TESTRAIL_PROJECT_ID || reporterOptions.testrailSestraProjectId || '';
      this.suiteId = process.env.TESTRAIL_SUITE_ID || reporterOptions.suiteId || '';

      // Optional
      const runName = process.env.TESTRAIL_RUN_NAME || reporterOptions.runName || 'Automation Run';
      const includeAllTestCaseIds = ((process.env.TESTRAIL_INCLUDE_ALL === true) || (process.env.TESTRAIL_INCLUDE_ALL === undefined)) &&
                                    ((reporterOptions.includeAll === true) || (reporterOptions.includeAll === undefined))
      const all_case_ids = []; // FIXME
      this.verbose = true; // FIXME add options

      if (this.areSettingsValid() === false) {
        return;
      }

      let testrail = new TestRailApi(this.host, this.user,this.password, this.verbose);

      const results = [];

      emitter.on('assertion', (err, o) => {
          const re = /\bC(\d+)\b/;
          const str = o.assertion.split(' ');
          const case_ids = str.filter(i => i.match(re)).map(i => i.substr(1));
          
          case_ids.forEach((case_id) => {
              results.push({
                  case_id: case_id,
                  status_id: (err) ? 5 : 1,
                  comment: (err) ? `Test failed: ${err.message}` : 'Test passed'
              });
              all_case_ids.push(case_id);
          });
      });

      emitter.on('request', (error, payload) => {
        if (error) {
          console.error(error);
          return;
        }
      
        //console.log('--- RESPONSE ---');
        //console.log(payload.response);
      });

      emitter.on('beforeDone', async (error) => {
          console.log('start');

          if (error) {
              console.error(error);
              return;
          }

          const runDetails = {
              projectId: this.projectId,
              name: `${runName} - NEW`,
              description: `${runName} - ${new Date().toISOString()}`,
              suite_id: this.suiteId,
              include_all: includeAllTestCaseIds,
              case_ids: all_case_ids
          }

          const runId = await testrail.addRun(runDetails);
          console.log('runId', runId);

          const result = await testrail.addResults(runId, results);
          console.log('result');
          //await testrail.closeRun(runId);

          console.log('end');
      });

  }

  areSettingsValid() {
    if (!(this.host)) {
      console.error('Please provide your Testrail server address.');
      return false;
    }
    if (!(this.user)) {
      console.error('Please provide your Testrail user (usually this is your email address).');
      return false;
    }
    if (!(this.password)) {
      console.error('Please provide your Testrail password or API key.');
      return false;
    }
    if (!(this.projectId)) {
      console.error('Please provide your Testrail project id.');
      return false;
    }

    return true;
  }
}

module.exports = TestRailSestraReporter;