const rp = require('request-promise');

module.exports = class TestRailApi {
    constructor(host, username, apiKey, verbose) {
      this.host = host;
      this.username = username;
      this.apiKey = apiKey;
      this.verbose = verbose;
      this.auth = Buffer.from(`${this.username}:${this.apiKey}`).toString('base64');
      this.requestHeaders = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + this.auth
      }
    }

    async addRun(runDetails) {
        if (this.verbose) {
            console.info('Adding new run: ', runDetails);
        }

        const requestOptions = {
          method: 'POST',
          uri: `${this.host}/index.php?/api/v2/add_run/${runDetails.projectId}`,
          headers: this.requestHeaders,
          body: {
            name: runDetails.name,
            description: runDetails.description
            // FIXME: add the rest of the fields
          },
          json: true
        };
       
        return rp(requestOptions)
            .then(testRun => testRun.id)
            .catch(function (error) {
                console.error('Creating the run failed.');
                if (this.verbose) {
                    console.info(error);
                }
            });
    }

    async addResults(runId, results) {
        if (this.verbose) {
            console.info('Adding test results: ', results);
        }

        const requestOptions = {
            method: 'POST',
            uri: `${this.host}/index.php?/api/v2/add_results_for_cases/${runId}`,
            headers: this.requestHeaders,
            body: { results: results },
            json: true
          };
         
          return rp(requestOptions)
              .catch(function (error) {
                  console.warn('Adding results failed.');
                  console.warn(error);
              });        
    }

    async closeRun(runId) {
        if (this.verbose) {
            console.info('Closing run id: ', runId);
        }

        const requestOptions = {
            method: 'POST',
            uri: `${this.host}/index.php?/api/v2/close_run/${runId}`,
            headers: this.requestHeaders,
          };
         
          return rp(requestOptions)
              //.then(testRun => testRun.id)
              .catch(function (error) {
                  console.warn('Closing test run failed.');
                  console.warn(error);
              }); 
    }
}
